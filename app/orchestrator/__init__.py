# -*-coding:utf8-*-

from fastapi import FastAPI
from starlette.middleware.gzip import GZipMiddleware
from starlette_exporter import handle_metrics, PrometheusMiddleware

from app.orchestrator import routes


def create_app():
    app = FastAPI()
    app.add_middleware(GZipMiddleware, minimum_size=1000)
    app.add_middleware(PrometheusMiddleware, app_name="personalizacion_api", group_paths=True)
    app.add_route("/metrics", handle_metrics)

    app.include_router(routes.router)
    return app
