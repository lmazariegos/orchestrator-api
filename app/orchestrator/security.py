import os

from fastapi import Depends, HTTPException
from fastapi.security import APIKeyHeader
from starlette.status import HTTP_401_UNAUTHORIZED

X_API_KEY = APIKeyHeader(name='x-api-key')


def get_api_key(x_api_key: str = Depends(X_API_KEY)):
    """ validates the x-api-key and raises a 401 if invalid """
    if x_api_key == os.environ['X_API_KEY']:
        return
    raise HTTPException(
        status_code=HTTP_401_UNAUTHORIZED,
        detail="Invalid API Key",
    )
